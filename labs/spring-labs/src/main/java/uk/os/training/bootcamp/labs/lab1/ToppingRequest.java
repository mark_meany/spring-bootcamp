package uk.os.training.bootcamp.labs.lab1;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ToppingRequest {
    private String name;
    private Integer initialStock;
    private Integer pencePerPortion;
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getInitialStock() {
        return initialStock;
    }
    public void setInitialStock(Integer initialStock) {
        this.initialStock = initialStock;
    }
    public Integer getPencePerPortion() {
        return pencePerPortion;
    }
    public void setPencePerPortion(Integer pencePerPortion) {
        this.pencePerPortion = pencePerPortion;
    }
}
