package uk.os.training.bootcamp.labs.lab5;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProcessPizzaEndpoint {

    // Two endpoints here:
    // '/price' will calculate the price of a Pizza's toppings
    // '/order' will update the stock values of all toppings
    // Both endpoints will take a PizzaRequest that contains a list of toppings and the number of portions required
    // There is to be no direct database access, use the other endpoints to obtain the information needed and perform updates
    // Yes - there will be a RestTemplate in use here
    
}
