package uk.os.training.bootcamp.labs.lab1;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreateToppingEndpoint {

    // Needs a method to handle POST requests in JSON or XML format
    // The methods should take an object of type ToppingRequest
    // The method does not need to return the toping name and a success indicator
    // If the toping was successfully added then return code should be CREATED
    
}
