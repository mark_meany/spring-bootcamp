package uk.os.training.bootcamp.labs.lab4;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeleteToppingEndpoint {

    // '/topping/{name}' DELETE endpoint
    // Do not care if not in database so dont error if not present
}
