package uk.os.training.bootcamp.labs.lab6;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class AggregationEndpoint {

    // Do we need this?
    // Acts as a proxy to other endpoints
    // Purpose is to isolate external access from endpoint implementation
}
