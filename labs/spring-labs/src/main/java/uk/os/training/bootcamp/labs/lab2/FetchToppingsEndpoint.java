package uk.os.training.bootcamp.labs.lab2;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class FetchToppingsEndpoint {

    // There will actually be two endpoints here, one that returns details about a specific topping
    // and another that returns a list of all toppings
    // '/topping/{name}' will be path for a specific topping
    // '/topping' will be path for a list of all toppings
}
