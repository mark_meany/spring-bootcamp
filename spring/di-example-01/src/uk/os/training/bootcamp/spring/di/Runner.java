package uk.os.training.bootcamp.spring.di;

public class Runner {

	public static void main(String[] args) {
		// Configure our application
		final GreetingService greetingService = new BasicGreetingService();
		final Greeter greeter = new Greeter(greetingService);

		System.out.println(greeter.greet("Mark"));
	}
}
