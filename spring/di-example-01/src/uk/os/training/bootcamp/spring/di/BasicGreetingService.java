package uk.os.training.bootcamp.spring.di;

public class BasicGreetingService implements GreetingService {

	@Override
	public String createGreeting(String name) {
		return String.format("Hello %s!", name);
	}

}
