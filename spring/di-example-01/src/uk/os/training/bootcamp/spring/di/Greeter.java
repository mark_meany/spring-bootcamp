package uk.os.training.bootcamp.spring.di;

public class Greeter {

    private final GreetingService greetingService;

    public Greeter(final GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String greet(String name) {
        return greetingService.createGreeting(name);
    }
}
