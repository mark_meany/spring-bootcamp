package uk.os.training.bootcamp.spring.validation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Runner {

    public static void main(String[] args) {
        try (final AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext("uk.os.training");) {
            
            final UserService service = ctx.getBean(UserService.class);
            
            User user = new User("mmeany", "harharhar", "begone:gmail,com");
            service.storeUser(user);

            System.out.println("--------");
            
            user = new User("mmeany12345", "harharhar-long", "begone@gmail.com");
            service.storeUser(user);
        }
    }
}
