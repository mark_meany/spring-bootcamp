package uk.os.training.bootcamp.spring.validation;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private Validator validator;
    
    public void storeUser(final User user) {
        
        Set<ConstraintViolation<User>> results = validator.validate(user);
        
        if (results.isEmpty()) {
            System.out.println("No validation violations!");
        }
        for (ConstraintViolation<User> c : results) {
            System.out.println(String.format("Field '%s' failed validation: %s", c.getPropertyPath(), c.getMessage()));
        }
    }
}
