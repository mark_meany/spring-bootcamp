package uk.os.training.bootcamp.spring.di;

public interface GreetingService {

	String createGreeting(String name);

}
