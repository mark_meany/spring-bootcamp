package uk.os.training.bootcamp.spring.di;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Runner {

	public static void main(String[] args) {
		// Configure our application
		try (final AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext("uk.os.training");) {
			final Greeter greeter = ctx.getBean(Greeter.class);
			System.out.println(greeter.greet("Mark"));
		}
	}
}
