package uk.os.training.bootcamp.spring.di;

import org.springframework.stereotype.Component;

@Component
public class Greeter {

	private final GreetingService greetingService;
	
	public Greeter(final GreetingService greetingService) {
		this.greetingService = greetingService;
	}
	
	public String greet(String name) {
		return greetingService.createGreeting(name);
	}
}
