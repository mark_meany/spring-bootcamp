package uk.os.training.bootcamp.spring.di;

import org.springframework.stereotype.Service;

@Service
public class BasicGreetingService implements GreetingService {

	public String createGreeting(String name) {
		return String.format("Hello %s!", name);
	}
}
