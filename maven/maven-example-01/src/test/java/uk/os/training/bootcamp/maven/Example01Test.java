package uk.os.training.bootcamp.maven;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Example01Test {

	private Example01 sut = new Example01();

	@Test
	public void oneAddOneMakesTwo() {
		assertEquals("Expect 1 + 1 = 2", 2, sut.add(1, 1));
	}

	@Test
	public void canHaveNegativeResult() {
		assertEquals("Expect 1 - 3 = -2", -2, sut.add(1, -3));
	}
}
