package uk.os.training.bootcamp.maven;

public class Example01 {

    public int add(int a, int b) {
        return a + b;
    }

    public static final void main(final String[] args) {
        final Example01 me = new Example01();
        final int a = Integer.valueOf(args[0]);
        final int b = Integer.valueOf(args[1]);
        System.out.format("%d + %d = %d\n", a, b, me.add(a,b));
    }
}