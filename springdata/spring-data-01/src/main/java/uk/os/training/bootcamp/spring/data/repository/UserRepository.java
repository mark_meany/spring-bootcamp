package uk.os.training.bootcamp.spring.data.repository;

import org.springframework.data.repository.CrudRepository;

import uk.os.training.bootcamp.spring.data.entity.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

    UserEntity findOneByUsername(String username);
}
