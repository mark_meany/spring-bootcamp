package uk.os.training.bootcamp.spring.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.os.training.bootcamp.spring.data.entity.UserEntity;
import uk.os.training.bootcamp.spring.data.repository.UserRepository;

@SpringBootApplication
public class SpringData01Application {

	public static void main(String[] args) throws Exception {
		ConfigurableApplicationContext ctx = SpringApplication.run(SpringData01Application.class, args);

		ObjectMapper om = new ObjectMapper();
		
		UserRepository repo = ctx.getBean(UserRepository.class);
		
		UserEntity user = new UserEntity();
		user.setActive(true);
		user.setUsername("mmeany");
		user.setPassword("password1");
		user.setEmail("mark.meany@os.uk");
		
		UserEntity saved = repo.save(user);
		
        System.out.println(om.writeValueAsString(saved));
		
		System.out.println("The users table contains: " + repo.count() + " entries.");

		user = new UserEntity();
		user.setActive(true);
        user.setUsername("tsmart");
        user.setPassword("password2");
        user.setEmail("tbone.smart@os.uk");
		
        saved = repo.save(user);
        
        System.out.println(om.writeValueAsString(saved));

        System.out.println("The users table contains: " + repo.count() + " entries.");

        user = repo.findOne(1L);
        user.setPassword("newPassword");
        saved = repo.save(user);
        
        System.out.println(om.writeValueAsString(saved));

        System.out.println("The users table contains: " + repo.count() + " entries.");
        
		user = repo.findOneByUsername("tsmart");
        System.out.println("Found: " + om.writeValueAsString(user));
		
	}
}
