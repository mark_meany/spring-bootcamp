package uk.os.training.bootcamp.spring.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="APP_USERS")
public class UserEntity {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="USR_ID")
    private Long id;
    
    @Version
    @Column(name="USR_VERSION")
    private Integer version;
    
    @Column(name="USR_USERNAME", length=20, nullable=false, unique=true)
    private String username;

    @Column(name="USR_PASSWORD", length=20, nullable=false)
    private String password;
    
    @Column(name="USR_EMAIL", length=120, nullable=false)
    private String email;
    
    @Column(name="USR_ACTIVE", nullable=false)
    private boolean active;

    public Long getId() {
        return id;
    }

/*    public void setId(Long id) {
        this.id = id;
    }
*/
    public Integer getVersion() {
        return version;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
