package uk.os.training.bootcamp.spring.client;

import java.util.ArrayList;
import java.util.List;

public class ListResponse {

    List<Integer> numbers = new ArrayList<>();

    public List<Integer> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<Integer> numbers) {
        this.numbers = numbers;
    }
}
