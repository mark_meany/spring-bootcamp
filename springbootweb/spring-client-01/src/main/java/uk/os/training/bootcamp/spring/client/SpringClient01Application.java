package uk.os.training.bootcamp.spring.client;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SpringClient01Application {

	public static void main(String[] args) {
		
		RestTemplate restTemplate = new RestTemplate();
		
		// This is how to configure headers for the rest template
        HttpHeaders xmlHeaders = new HttpHeaders();
        xmlHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
        HttpEntity<String> xmlEntity = new HttpEntity<String>("parameters", xmlHeaders);

        // Headers for retrieving JSON
        HttpHeaders jsonHeaders = new HttpHeaders();
        jsonHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> jsonEntity = new HttpEntity<String>("parameters", jsonHeaders);
        
        // Url of the list endpoint
        final String listEndpoint = "http://localhost:8080/list";

        // Fetch XML as a String
        System.out.println("--------- Fetch as XML");
        ResponseEntity<String> r0 = restTemplate.exchange(listEndpoint, HttpMethod.GET, xmlEntity, String.class);
        System.out.println(r0.getStatusCode());
        System.out.println(r0.getBody());
        
        // Fetch JSON as a String
        System.out.println("--------- Fetch as JSON");
        ResponseEntity<String> r1 = restTemplate.exchange(listEndpoint, HttpMethod.GET, jsonEntity, String.class);
        System.out.println(r1.getStatusCode());
        System.out.println(r1.getBody());

        // Fetch result and marshall as ListResponse
        System.out.println("--------- Fetch and marshall into an object");
        ResponseEntity<ListResponse> r2 = restTemplate.getForEntity(listEndpoint, ListResponse.class);
        System.out.println(r2.getStatusCode());
        for(Integer i : r2.getBody().numbers) {
            System.out.print(i + " | ");
        }
        System.out.println();

        // An entity that will ensure we receive data in JSON format
        HttpEntity<KvpResponse> kvpEntity = new HttpEntity<KvpResponse>(new KvpResponse(), jsonHeaders);

        // Endpoint template, parameters will be passed to RestTemplate to resolve placeholders - for demonstration in this case
        final String kvpEndpointTemplate = "http://localhost:8080/kvp/{key}";

        // Values that will be used to resolve placeholders in endpoint template
        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("key", "name");
        
        // Delete
        System.out.println("--------- DELETE (Should not be anything there!)");
        ResponseEntity<KvpResponse> r3 = restTemplate.exchange(kvpEndpointTemplate, HttpMethod.DELETE, kvpEntity, KvpResponse.class, pathVariables);
        System.out.println(r3.getStatusCode());
        System.out.println(r3.getBody().getKvp().getValue());
        
        // Post data to an endpoint
        System.out.println("--------- POST");
        HttpEntity<String> postEntity = new HttpEntity<String>("Mark", xmlHeaders);
        ResponseEntity<KvpResponse> r4 = restTemplate.exchange(kvpEndpointTemplate, HttpMethod.POST, postEntity, KvpResponse.class, pathVariables);
        System.out.println(r4.getStatusCode());
        System.out.println(r4.getBody().getKvp().getValue());

        // Read data from an endpoint
        System.out.println("--------- GET");
        ResponseEntity<KvpResponse> r5 = restTemplate.exchange(kvpEndpointTemplate, HttpMethod.GET, kvpEntity, KvpResponse.class, pathVariables);
        System.out.println(r5.getStatusCode());
        System.out.println(r5.getBody().getKvp().getValue());

        // Put data to an endpoint - do an update
        System.out.println("--------- PUT");
        HttpEntity<String> putEntity = new HttpEntity<String>("John", jsonHeaders);
        ResponseEntity<KvpResponse> r6 = restTemplate.exchange(kvpEndpointTemplate, HttpMethod.PUT, putEntity, KvpResponse.class, pathVariables);
        System.out.println(r6.getStatusCode());
        System.out.println(r6.getBody().getKvp().getValue());

        // Read data from an endpoint
        System.out.println("--------- GET");
        ResponseEntity<KvpResponse> r7 = restTemplate.exchange(kvpEndpointTemplate, HttpMethod.GET, kvpEntity, KvpResponse.class, pathVariables);
        System.out.println(r7.getStatusCode());
        System.out.println(r7.getBody().getKvp().getValue());

        // Delete
        System.out.println("--------- DELETE");
        ResponseEntity<KvpResponse> r8 = restTemplate.exchange(kvpEndpointTemplate, HttpMethod.DELETE, kvpEntity, KvpResponse.class, pathVariables);
        System.out.println(r8.getStatusCode());
        System.out.println(r8.getBody().getKvp().getValue());
        
	}
}
