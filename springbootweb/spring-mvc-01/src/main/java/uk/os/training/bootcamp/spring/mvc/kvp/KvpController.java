package uk.os.training.bootcamp.spring.mvc.kvp;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KvpController {

    private ConcurrentHashMap<String, String> cache = new ConcurrentHashMap<>();
    
    @RequestMapping(value="/kvp/{key}", method=RequestMethod.POST, produces={MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<KvpResponse> addToCache(@PathVariable("key") String key, @RequestBody String value) {
        
        boolean success = !cache.containsKey(key);
        HttpStatus status = success ? HttpStatus.OK : HttpStatus.BAD_REQUEST;
        if (success) {
            cache.put(key, value);
        }
        
        return new ResponseEntity<KvpResponse>(new KvpResponse(success, new KeyValue(key, value)), status);
    }
    
    @RequestMapping(value="/kvp/{key}", method=RequestMethod.PUT, produces={MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<KvpResponse> updateCache(@PathVariable("key") String key, @RequestBody String value) {
        
        boolean success = cache.containsKey(key);
        HttpStatus status = success ? HttpStatus.OK : HttpStatus.BAD_REQUEST;
        if (success) {
            cache.put(key, value);
        }
        
        return new ResponseEntity<KvpResponse>(new KvpResponse(success, new KeyValue(key, value)), status);
    }
    
    @RequestMapping(value="/kvp/{key}", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<KvpResponse> fromCache(@PathVariable("key") String key) {
        boolean success;
        KeyValue kvp;
        HttpStatus status;
        if (cache.containsKey(key)) {
            success = true;
            final String value = cache.get(key);
            kvp = new KeyValue(key, value);
            status = HttpStatus.OK;
        } else {
            success = false;
            kvp = new KeyValue(key, null);
            status = HttpStatus.NOT_FOUND;
        }
           
        return new ResponseEntity<KvpResponse>(new KvpResponse(success, kvp), status);
    }

    @RequestMapping(value="/kvp/{key}", method=RequestMethod.DELETE, produces={MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<KvpResponse> deleteFromCache(@PathVariable("key") String key) {
        
        String value = cache.remove(key);
        
        return new ResponseEntity<KvpResponse>(new KvpResponse(true, new KeyValue(key, value)), HttpStatus.OK);
    }
    
}
