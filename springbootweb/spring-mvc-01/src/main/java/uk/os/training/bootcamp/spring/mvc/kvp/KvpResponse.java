package uk.os.training.bootcamp.spring.mvc.kvp;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class KvpResponse {

    private boolean success;
    private KeyValue kvp;

    public KvpResponse() {
    }
    
    public KvpResponse(boolean success, KeyValue kvp) {
        super();
        this.success = success;
        this.kvp = kvp;
    }

    public boolean isSuccess() {
        return success;
    }
    
    public void setSuccess(boolean success) {
        this.success = success;
    }
    
    public KeyValue getKvp() {
        return kvp;
    }
    
    public void setKvp(KeyValue kvp) {
        this.kvp = kvp;
    }
    
}
