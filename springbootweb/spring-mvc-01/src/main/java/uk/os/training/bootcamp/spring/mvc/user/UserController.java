package uk.os.training.bootcamp.spring.mvc.user;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @RequestMapping(value="/user/{id}", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE}, produces={MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<UserResponse> postUser(@PathVariable("id") Integer id, @Valid @RequestBody User user, BindingResult bindings) {
        UserResponse response = new UserResponse();
        HttpStatus status;
        response.setUser(user);

        // Check result of validation
        if (bindings.hasErrors()) {
            response.setMessage(bindings.toString());
            status = HttpStatus.BAD_REQUEST;
        } else {
            status = HttpStatus.OK;
        }
        
        return new ResponseEntity<UserResponse>(response, status);
    }
    
}
