package uk.os.training.bootcamp.spring.mvc.add;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdditionController {

    // I need annotating
    public int add(String first, String second) {
        Integer a = new Integer(first);
        Integer b = new Integer(second);
        return a + b;
    }
    
}
