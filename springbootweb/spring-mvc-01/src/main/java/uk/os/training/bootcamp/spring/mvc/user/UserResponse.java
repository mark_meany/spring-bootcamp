package uk.os.training.bootcamp.spring.mvc.user;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserResponse {
    User user;
    String message;
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}
