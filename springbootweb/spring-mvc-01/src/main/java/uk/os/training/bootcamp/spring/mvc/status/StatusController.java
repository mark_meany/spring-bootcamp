package uk.os.training.bootcamp.spring.mvc.status;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatusController {

    @RequestMapping(value="/status", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<StatusResponse> status() {
        return new ResponseEntity<StatusResponse>(new StatusResponse("OK"), HttpStatus.OK);
    }
}