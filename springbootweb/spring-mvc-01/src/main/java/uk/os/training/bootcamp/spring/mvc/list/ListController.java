package uk.os.training.bootcamp.spring.mvc.list;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ListController {

    @RequestMapping(value="/list", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<ListResponse> getList(@RequestParam(name="maximum", defaultValue="10", required=false) int max) {
        final ListResponse response = new ListResponse();
        for(int i = 0; i < max; i++) {
            response.getNumbers().add(i + i);
        }
        return new ResponseEntity<ListResponse>(response, HttpStatus.OK);
    }
}
