package uk.os.training.bootcamp.spring.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfiguration {

    // We can perform injection into @Configuration classes!
    @Value("${my.message}")
    private String message;
    
    // Annotated with Bean to add to Context
    @Bean
    public MessageService messageService() {
        return new MessageService(message);
    }
    
}
