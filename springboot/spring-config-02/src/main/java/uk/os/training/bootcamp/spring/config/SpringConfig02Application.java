package uk.os.training.bootcamp.spring.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringConfig02Application {

	public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(SpringConfig02Application.class, args);
        MessageService service = ctx.getBean(MessageService.class);
        System.out.println(service.theMessage());
	}
}
