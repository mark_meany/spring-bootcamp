package uk.os.training.bootcamp.spring.config;

public class MessageService {

    private final String message;
    
    public MessageService(final String message) {
        this.message = message;
    }
    
    public String theMessage() {
        return message;
    }
    
}
