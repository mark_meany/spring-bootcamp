package uk.os.training.bootcamp.spring.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    @Value("${my.message}")
    private String message;
    
    public String theMessage() {
        return message;
    }
    
}
